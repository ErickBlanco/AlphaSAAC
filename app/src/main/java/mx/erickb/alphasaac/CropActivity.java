package mx.erickb.alphasaac;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * ErickB on 08/10/2017.
 */

@SuppressLint("Registered")
public class CropActivity extends AppCompatActivity {

    // Codes
    private static final int SELECT_PICTURE = 101;
    private static final String CROPPED_IMAGE_NAME = "CropImage.jpeg";

    /**
     * Creates an intent to get image files and wait the result
     */
    void takePicture() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, SELECT_PICTURE);
    }

    /**
     * Initialize the UCrop constructor, launch the cropper and wait a result
     *
     * @param uri the URI from the image to crop.
     */
    private void startCropActivity(@NonNull Uri uri) {
        Uri mDestinationUri = Uri.fromFile(new File(getCacheDir(), CROPPED_IMAGE_NAME));
        UCrop.of(uri, mDestinationUri)
                .withAspectRatio(1, 1)
                .withMaxResultSize(450, 450)
                .start(this);
    }

    /**
     * Gets the result from the cropper and converts it to a Bitmap object
     *
     * @param result the result from the cropper activity
     */
    private void handleCropResult(@NonNull Intent result) {
        final Uri resultUri = UCrop.getOutput(result);
        if (resultUri == null)
            return;

        Bitmap bm = null;
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        try {
            bm = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (bm != null)
            bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        bitmapResult(bm);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCropActivity(data.getData());
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                handleCropResult(data);
            }
        }
    }

    /**
     * @param c the actual context
     * @return a new File in DIRECTORY_PICTURES
     * @throws IOException something went wrong when creating the file
     */
    static File createFile(Context c) throws IOException {
        String fileName = "FILE_" + System.currentTimeMillis();
        File storageDir = c.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(
                fileName,
                ".jpg",
                storageDir
        );

    }

    // Empty method to manage the crop result in extended classes
    void bitmapResult(Bitmap result) {
        // Fill with your own code
    }
}
