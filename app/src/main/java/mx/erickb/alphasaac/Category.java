package mx.erickb.alphasaac;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mx.erickb.pictogram.Pictodata;
import mx.erickb.pictogram.Pictogram;

@SuppressWarnings("unused")
public class Category extends AppCompatActivity
        implements FloatingActionButton.OnClickListener,
        GridView.OnItemClickListener {

    // Extra's id
    public static final String ID = "cat_id";
    public static final String TITLE = "act_tittle";

    // Activity views
    private GridView mPictogram;
    private FloatingActionButton mFAB;

    // Data instances
    private ArrayList<Pictodata> mList;

    // Auxiliary variables
    private int categoryID;
    private boolean menuVisibility = false, fabVisibility = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        // Get the title of the category
        String barTitle = getIntent().getStringExtra(TITLE);
        setTitle(barTitle);

        // Initialize classes
        mList = new ArrayList<>();

        // Get view references
        mPictogram = findViewById(R.id.gridDisplay);
        mFAB = findViewById(R.id.fab);
        mFAB.setOnClickListener(this);
        newEntryVisible(fabVisibility);

        // Get the ID of the category, used to find and store elements
        categoryID = getIntent().getIntExtra(ID, -1);
        initView(categoryID);
    }

    /**
     * Access to the database and get all the elements with
     * a certain category ID and fills the GridView with them
     *
     * @param id the ID of the database registers
     */
    private void initView(int id) {
        DatabaseHelper helper = DatabaseHelper.getInstance(this);
        Cursor cursor = helper.getFromCategory(id);
        mList.clear();

        if (cursor.moveToFirst()) {
            do {
                String title = cursor.getString(0);
                String description = cursor.getString(1);
                String path = cursor.getString(2);
                int item = cursor.getInt(3);

                mList.add(new Pictodata(item, title, description, path));

            } while (cursor.moveToNext());
        }
        cursor.close();

        PictogramAdapter adapter = new PictogramAdapter<>(this, mList, menuVisibility);
        mPictogram.setAdapter(adapter);
        mPictogram.setOnItemClickListener(this);
    }

    /**
     * Called when the '+' button is pressed, it calls the
     * PictogramManager activity with no extra params
     */
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, PictogramManager.class);
        intent.putExtra(PictogramManager.CATEGORY_ID, categoryID);
        startActivity(intent);
    }

    /**
     * Sets the FloatingActionButton's visibility
     *
     * @param visible the new visibility
     */
    private void newEntryVisible(boolean visible) {
        int visibility = visible ? View.VISIBLE : View.GONE;
        mFAB.setVisibility(visibility);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Pictogram test = view.findViewById(R.id.editable);
        String msg = test.getDescription();
        AAC.saySomething(msg);
    }

    private class PictogramAdapter<T> extends ArrayAdapter<T> {

        private final Context mContext;
        private boolean menuVisibility;

        private PictogramAdapter(Context context, List<T> items, boolean menu) {
            super(context, 0, items);
            mContext = context;
            menuVisibility = menu;
        }

        @Override
        @NonNull
        public View getView(int position, View view, @NonNull ViewGroup parent) {

            if (view == null) {
                LayoutInflater inflater = (LayoutInflater)
                        mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.grid_element, parent, false);
            }

            final Pictodata data = (Pictodata) getItem(position);
            Pictogram editable = view.findViewById(R.id.editable);

            Pictogram.MenuItemSelected listener = new Pictogram.MenuItemSelected() {
                @Override
                public void itemPressed(int code) {
                    if (code == Pictogram.EDIT) {
                        Intent intent = new Intent(getContext(), PictogramManager.class);
                        intent.putExtra(PictogramManager.CATEGORY_ID, categoryID);
                        if (data != null)
                            intent.putExtra(PictogramManager.RESOURCE, data.getID());
                        startActivity(intent);
                    }
                    if (code == Pictogram.DELETE) {
                        DatabaseHelper helper = DatabaseHelper.getInstance(mContext);
                        if (data != null) {
                            if (new File(data.getImagePath()).delete()) {
                                Log.v(getClass().getName(), "File erased");
                            }
                            helper.deleteItem(data.getID());
                        }
                        initView(categoryID);
                    }

                }
            };
            editable.setMenuListener(listener);

            if (data != null) {
                editable.setPictogramImage(data.getBitmap());
                editable.setTitleText(data.getTitle());
                editable.setDescription(data.getDescription());
                editable.setMenuVisibility(menuVisibility);
            }

            return view;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.m_add) {
            fabVisibility = !fabVisibility;
            newEntryVisible(fabVisibility);
        }
        if (id == R.id.m_menus) {
            menuVisibility = !menuVisibility;
            initView(categoryID);
        }

        return true;
    }

    @Override
    protected void onResume() {
        // when the activity is resumed in order to refresh the view
        initView(categoryID);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        // stop the TTS service
        AAC.silenceSpeech();
        super.onDestroy();
    }
}
