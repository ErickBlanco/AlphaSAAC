package mx.erickb.alphasaac;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import mx.erickb.mainbutton.MainButton;

@SuppressWarnings("unused")
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void launchActivity(View v) {
        Intent intent = new Intent(this, Category.class);

        // Uses the id of the button as category identifier in the database
        int id = v.getId();
        intent.putExtra(Category.ID, id);

        // Uses the name of the button as title in the Category activity
        String title = ((MainButton) v).getItemTitle();
        intent.putExtra(Category.TITLE, title);

        startActivity(intent);
    }

}
