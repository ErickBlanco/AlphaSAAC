package mx.erickb.alphasaac;

import android.app.Application;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.Locale;

/**
 * ErickB on 12/10/2017.
 */

public class AAC extends Application
        implements TextToSpeech.OnInitListener {

    private static TextToSpeech mTts;

    @Override
    public void onCreate() {
        super.onCreate();

        // Creates an instance of the system default TextToSpeech service
        mTts = new TextToSpeech(this, this);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            mTts.setLanguage(new Locale("ES", "MX"));
        } else {
            Log.e("SpeakService", "Could not initialize TextToSpeech.");
        }
    }

    /**
     * Provides an access method to TextToSpeech for
     * the entire application
     *
     * @param message Text to synthesize
     */
    @SuppressWarnings("deprecation")
    public static void saySomething(String message) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            mTts.speak(message, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            mTts.speak(message, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    /**
     * Allow to stop the TextToSpeech if is
     * playing something
     */
    public static void silenceSpeech() {
        if (mTts.isSpeaking())
            mTts.stop();
    }
}