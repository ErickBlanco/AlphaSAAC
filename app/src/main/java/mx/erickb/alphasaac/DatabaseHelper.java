package mx.erickb.alphasaac;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import mx.erickb.pictogram.Pictodata;

/**
 * ErickB on 03/10/2017.
 */
@SuppressWarnings("unused")
class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper mInstance = null;
    private static SQLiteDatabase mDatabase = null;

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "db_alpha_data";

    // Table name
    private static final String TB_ITEMS = "db_items";

    // Column names
    private static final String KEY_ID = "db_id";
    private static final String KEY_TITLE = "db_title";
    private static final String KEY_DESCRIPTION = "db_description";
    private static final String KEY_IMAGE = "db_img_path";
    private static final String KEY_CATEGORY = "db_category";

    static DatabaseHelper getInstance(Context ctx) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (mInstance == null) {
            mInstance = new DatabaseHelper(ctx.getApplicationContext());
        }
        return mInstance;
    }

    private DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mDatabase = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + TB_ITEMS
                        + "("
                        + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + KEY_CATEGORY + " INTEGER, "
                        + KEY_TITLE + " TEXT, "
                        + KEY_DESCRIPTION + " TEXT, "
                        + KEY_IMAGE + " TEXT "
                        + ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TB_ITEMS);
        onCreate(db);
    }

    /**
     * Creates a new row from a pictogram element and stores in the database
     *
     * @param category    Category of the element
     * @param name        Title of the pictogram
     * @param description Content description for TTS
     * @param image       Pictogram's bitmap
     */
    void insertItem(int category, String name, String description, String image) {
        //SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_CATEGORY, category);
        contentValues.put(KEY_TITLE, name);
        contentValues.put(KEY_DESCRIPTION, description);
        contentValues.put(KEY_IMAGE, image);

        //db.insert(TB_ITEMS, null, contentValues);
        mDatabase.insert(TB_ITEMS, null, contentValues);
    }

    /**
     * Provides the current number of elements in the database
     *
     * @return The number of rows in the table
     */
    public int numberOfRows() {
        //SQLiteDatabase db = this.getReadableDatabase();
        //return (int) DatabaseUtils.queryNumEntries(db, TB_ITEMS);
        return (int) DatabaseUtils.queryNumEntries(mDatabase, TB_ITEMS);
    }

    /**
     * Reads and replace new data from an existing element
     *
     * @param id          The id of the element to modify
     * @param name        The new tittle of the pictogram
     * @param description The new pictogram's content description
     * @param image       New pictogram bitmap
     */
    void updateItem(int id, String name, String description, String image) {
        //SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_TITLE, name);
        contentValues.put(KEY_DESCRIPTION, description);
        contentValues.put(KEY_IMAGE, image);

        mDatabase.update(
                TB_ITEMS,
                contentValues,
                KEY_ID + " = ? ",
                new String[]{Integer.toString(id)});
    }

    /**
     * Deletes an existing element in the table
     *
     * @param id The id of the element to erase
     */
    void deleteItem(int id) {
        //SQLiteDatabase db = this.getWritableDatabase();
        mDatabase.delete(TB_ITEMS,
                KEY_ID + " = ? ",
                new String[]{Integer.toString(id)});
    }

    /**
     * Get all the elements belonging to a certain category
     *
     * @param category The category to find
     * @return The element resulting from the search
     */
    Cursor getFromCategory(int category) {
        //SQLiteDatabase db = this.getReadableDatabase();

        String[] columns = new String[]{KEY_TITLE, KEY_DESCRIPTION, KEY_IMAGE, KEY_ID};
        String[] arg = new String[]{"" + category};
        String selection = KEY_CATEGORY + "=?";

        return mDatabase.query(TB_ITEMS, columns, selection, arg, null, null, null);
    }

    /**
     * Search for an specific element id an retrieves its data
     *
     * @param id the id of the element in the table
     * @return an object containing all the relevant attributes
     */
    Pictodata getElementByID(int id) {
        //SQLiteDatabase db = this.getReadableDatabase();

        String[] columns = new String[]{KEY_TITLE, KEY_DESCRIPTION, KEY_IMAGE};
        String[] arg = new String[]{"" + id};
        String selection = KEY_ID + "=?";

        Cursor data = mDatabase.query(TB_ITEMS, columns, selection, arg, null, null, null);
        data.moveToFirst();

        String title = data.getString(0);
        String description = data.getString(1);
        String img = data.getString(2);

        data.close();
        return new Pictodata(id, title, description, img);
    }
}
