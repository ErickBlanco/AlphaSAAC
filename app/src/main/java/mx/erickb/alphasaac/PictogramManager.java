package mx.erickb.alphasaac;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import mx.erickb.pictogram.Pictodata;

public class PictogramManager extends CropActivity
        implements View.OnClickListener {

    // Extra's id
    public static final String CATEGORY_ID = "cat_id";
    public static final String RESOURCE = "pic_class";

    // Views
    private TextInputEditText mTitle, mDescription;
    private Button mSelect, mDone;
    private ImageView mImage;
    private DatabaseHelper helper;
    private Bitmap bitmap;
    private int catID, resID;
    private String imgPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictogram_manager);

        getReferences();
        mSelect.setOnClickListener(this);
        mDone.setOnClickListener(this);

        helper = DatabaseHelper.getInstance(this);
        catID = getIntent().getIntExtra(CATEGORY_ID, -1);
        resID = getIntent().getIntExtra(RESOURCE, -1);

        if (resID != -1) {
            Pictodata tempData = helper.getElementByID(resID);
            mTitle.setText(tempData.getTitle());
            mDescription.setText(tempData.getDescription());
            bitmap = tempData.getBitmap();
            mImage.setImageBitmap(bitmap);
            imgPath = tempData.getImagePath();
        }
    }

    /**
     * Get the references from all the views in the activity layout
     */
    private void getReferences() {
        mTitle = findViewById(R.id.pm_title);
        mDescription = findViewById(R.id.pm_description);
        mSelect = findViewById(R.id.pm_select);
        mDone = findViewById(R.id.pm_save);
        mImage = findViewById(R.id.pm_image);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.pm_select) {
            takePicture();
        }
        if (id == R.id.pm_save) {
            if (!validateData()) {
                Toast.makeText(this, getString(R.string.pm_toast), Toast.LENGTH_SHORT).show();
                return;
            }
            // prevents multiple taps
            mDone.setEnabled(false);
            // If is just a modification updates the item instead of create a new one
            if (resID != -1) {
                // Updates the element with the primary key resID
                File file = new File(imgPath);
                if (file.delete()) {
                    Log.v(getClass().getName(), "File deleted");
                }
                String path = saveBitmap(bitmap);

                helper.updateItem(resID,
                        mTitle.getText().toString(),
                        mDescription.getText().toString(),
                        path);
            } else {
                // Inserts a new element with the category catID
                String path = saveBitmap(bitmap);
                helper.insertItem(catID,
                        mTitle.getText().toString(),
                        mDescription.getText().toString(),
                        path);
            }
            onBackPressed();
        }
    }

    /**
     * *Receives a bitmap and store it in the app folder
     *
     * @param bitmap the bitmap to store
     * @return the path to the file created
     */
    private String saveBitmap(Bitmap bitmap) {
        try {
            File file = createFile(this);
            FileOutputStream fos = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
            fos.close();
            return file.getAbsolutePath();

        } catch (IOException e) {
            e.printStackTrace();
            return "exception";
        }
    }

    /**
     * Check if the data is ready to create/update an element in the database
     *
     * @return true if everything is fine, false otherwise
     */
    private boolean validateData() {
        return mTitle.getText().length() > 0 && mDescription.getText().length() > 0 && (bitmap != null);
    }

    @Override
    protected void bitmapResult(Bitmap result) {
        // save the result from the cropper and refresh the ImageView
        bitmap = result;
        mImage.setImageBitmap(result);
    }
}
