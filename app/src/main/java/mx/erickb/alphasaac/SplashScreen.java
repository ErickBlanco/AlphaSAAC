package mx.erickb.alphasaac;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.race604.drawable.wave.WaveDrawable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import mx.erickb.pictogram.Pictodata;

public class SplashScreen extends AppCompatActivity {

    private static final String DB_STATUS = "db_initiated";

    private DatabaseHelper helper;
    private AssetManager manager;
    private WaveDrawable waveDrawable;

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_splash_screen);

        helper = DatabaseHelper.getInstance(this);
        manager = getAssets();

        waveDrawable = new WaveDrawable(this, R.mipmap.ic_launcher);
        ImageView imageView = findViewById(R.id.splash_icon);
        imageView.setImageDrawable(waveDrawable);

        waveDrawable.setWaveAmplitude(8);

        SharedPreferences check = getPreferences(MODE_PRIVATE);
        if (!check.getBoolean(DB_STATUS, false)) {
            AsyncTask<Void, Integer, Boolean> task = new InitDatabase();
            task.execute();

            SharedPreferences.Editor editor = check.edit();
            editor.putBoolean(DB_STATUS, true);
            editor.apply();
        } else {
            Intent mainMenu = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(mainMenu);
            finish();
        }
    }

    private class InitDatabase extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            InputStream inputStream;
            OutputStream outputStream;
            File file;
            Context context = getApplicationContext();
            int progress = 0, increment = 10000 / Constants.data.length;

            for (Pictodata i : Constants.data) {
                try {
                    inputStream = manager.open(i.getImagePath());
                    file = CropActivity.createFile(context);
                    outputStream = new FileOutputStream(file);

                    byte buffer[] = new byte[1024];
                    int length;

                    while ((length = inputStream.read(buffer)) > 0) {
                        outputStream.write(buffer, 0, length);
                    }
                    outputStream.close();
                    inputStream.close();

                    helper.insertItem(
                            i.getID(),
                            i.getTitle(),
                            i.getDescription(),
                            file.getAbsolutePath());

                } catch (IOException e) {
                    e.printStackTrace();
                }

                progress += increment;
                publishProgress(progress);
            }
            helper.close();
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progress = values[0];
            waveDrawable.setLevel(progress);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(Boolean result) {
            Intent mainMenu = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(mainMenu);
            finish();
        }

        @Override
        protected void onCancelled() {

        }
    }
}
