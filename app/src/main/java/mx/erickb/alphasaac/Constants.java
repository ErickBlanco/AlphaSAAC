package mx.erickb.alphasaac;

import mx.erickb.pictogram.Pictodata;

/**
 * ErickB on 10/10/2017.
 */

class Constants {

    private interface Category {
        int SOCIAL = R.id.menu_social;
        int HOME = R.id.menu_home;
        int HEALTH = R.id.menu_health;
        int DOCTOR = R.id.menu_doctor;
        int PHARMACY = R.id.menu_pharmacy;
        int STORE = R.id.menu_store;
        int FOOD = R.id.menu_food;
        int MONEY = R.id.menu_money;
        int HOBBY = R.id.menu_hobby;
        int NEEDS = R.id.menu_needs;
    }

    static final Pictodata[] data = {
            // SOCIAL
            new Pictodata(Category.SOCIAL, "Hola", "Hola, ¿Cómo estás?", "init/social_hello.jpg"),
            new Pictodata(Category.SOCIAL, "Buenos días", "¡Buenos días!", "init/social_morning.jpg"),
            new Pictodata(Category.SOCIAL, "Buenas tardes", "¡Buenas tardes!", "init/social_evening.jpg"),
            new Pictodata(Category.SOCIAL, "Buenas noches", "¡Buenas noches!", "init/social_night.jpg"),
            new Pictodata(Category.SOCIAL, "Adiós", "Hasta luego", "init/social_bye.jpg"),
            new Pictodata(Category.SOCIAL, "Mucho gusto", "Mucho gusto", "init/social_happy.jpg"),

            // HOME
            new Pictodata(Category.HOME, "Desorden", "Está todo tirado", "init/home_mess.jpg"),
            new Pictodata(Category.HOME, "Cerrar", "Cierra la puerta", "init/home_door.jpg"),
            new Pictodata(Category.HOME, "Ventana", "Abre la ventana", "init/home_window.jpg"),
            new Pictodata(Category.HOME, "Televisión", "Prende la televisión", "init/home_television.jpg"),
            new Pictodata(Category.HOME, "Luces", "Apaga las luces", "init/home_lights.jpg"),
            new Pictodata(Category.HOME, "Llaves", "¿Dónde están las llaves?", "init/home_keys.jpg"),

            // HEALTH
            new Pictodata(Category.HEALTH, "Medicina", "Necesito mi medicina", "init/health_medicine.jpg"),
            new Pictodata(Category.HEALTH, "Mal", "Me siento mal", "init/health_bad.jpg"),
            new Pictodata(Category.HEALTH, "Presión", "Quiero tomarme la presión", "init/health_pressure.jpg"),
            new Pictodata(Category.HEALTH, "Azúcar", "Quiero medir el azúcar", "init/health_sugar.jpg"),
            new Pictodata(Category.HEALTH, "¿Cuál medicina?", "¿Qué medicamento me toca?", "init/health_which.jpg"),

            // DOCTOR
            new Pictodata(Category.DOCTOR, "Ir al doctor", "¿Pueden llevarme al doctor?", "init/doctor_doctor.jpg"),
            new Pictodata(Category.DOCTOR, "Horarios", "¿Cómo se toma la medicina?", "init/doctor_schedule.jpg"),
            new Pictodata(Category.DOCTOR, "Proxima visita", "¿Cuándo es la próxima cita?", "init/doctor_appointment.jpg"),

            // PHARMACY
            new Pictodata(Category.PHARMACY, "Receta", "Quiero surtir esta receta", "init/pharmacy_prescription.jpg"),
            new Pictodata(Category.PHARMACY, "Precio", "¿Cuál es el precio de la medicina?", "init/pharmacy_cost.jpg"),

            // STORE
            new Pictodata(Category.STORE, "Costo", "¿Cuánto cuesta?", "init/store_price.jpg"),
            new Pictodata(Category.STORE, "Total", "¿Cuánto va a ser?", "init/store_total.jpg"),
            new Pictodata(Category.STORE, "¿Dónde hay?", "¿Dónde está la tienda?", "init/store_where.jpg"),

            // MONEY
            new Pictodata(Category.MONEY, "Banco", "Necesito ir al banco", "init/money_bank.jpg"),
            new Pictodata(Category.MONEY, "No dinero", "No traigo dinero", "init/money_wallet.jpg"),
            new Pictodata(Category.MONEY, "Tarjeta de crédito", "Debo pagar la tarjeta", "init/money_credit.jpg"),
            new Pictodata(Category.MONEY, "La cuenta", "Hay que pagar la cuenta", "init/money_account.jpg"),
            new Pictodata(Category.MONEY, "La factura", "Hay que pagar la factura", "init/money_bill.jpg"),

            // FOOD
            new Pictodata(Category.FOOD, "Vamos a comer", "¿Vamos a comer?", "init/food_eat.jpg"),
            new Pictodata(Category.FOOD, "¿Qué hay?", "¿Qué hay de comer?", "init/food_which.jpg"),
            new Pictodata(Category.FOOD, "Más comida", "Más comida, por favor", "init/food_more.jpg"),

            // HOBBY
            new Pictodata(Category.HOBBY, "Salir a caminar", "¿Salimos a caminar?", "init/hobby_walk.jpg"),
            new Pictodata(Category.HOBBY, "Leer", "Iré a leer", "init/hobby_read.jpg"),
            new Pictodata(Category.HOBBY, "Libro nuevo", "Estoy buscando un libro", "init/hobby_book.jpg"),
            new Pictodata(Category.HOBBY, "Ciclismo", "Saldré en bicicleta", "init/hobby_cycling.jpg"),
            new Pictodata(Category.HOBBY, "Peliculas", "Quiero ver una película", "init/hobby_movie.jpg"),
            new Pictodata(Category.HOBBY, "Música", "Quiero escuchar música", "init/hobby_music.jpg"),

            // NEEDS
            new Pictodata(Category.NEEDS, "Baño", "Tengo que ir al baño", "init/need_bathroom.jpg"),
            new Pictodata(Category.NEEDS, "Agua", "Quiero agua", "init/need_water.jpg"),
            new Pictodata(Category.NEEDS, "Hambre", "Tengo hambre", "init/need_hungry.jpg"),
            new Pictodata(Category.NEEDS, "Sueño", "Necesito dormir", "init/need_sleep.jpg")
    };
}
